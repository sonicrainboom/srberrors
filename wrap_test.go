package srberrors

import (
	"errors"
	"gorm.io/gorm/utils/tests"
	"log"
	"regexp"
	"testing"
)

func TestWrap(t *testing.T) {
	err := errors.New("external error")
	wrapped := Wrap(err, "message1", "message2")

	output := regexp.MustCompile(`(?m)^message1 message2: external error \[ (?:.+)srberrors/wrap_test.go:\d+ \(srberrors.TestWrap\) \]$`)

	tests.AssertEqual(t, output.MatchString(wrapped.Error()), true)
	tests.AssertEqual(t, CombinedErrorType(wrapped), ETypeUnflaggedExternal)
	tests.AssertEqual(t, errors.Unwrap(wrapped), err)
}

func TestNestedWrap(t *testing.T) {
	err := New("internal error", ETypeExec)
	wrapped := NestedWrap(err, -1, "message1", "message2")

	output := regexp.MustCompile(`(?m)^message1 message2: internal error \[ (?:.+)srberrors/wrap.go:\d+ \(srberrors.NestedWrap\) \]$`)

	log.Printf("Want: %s\n", output)
	log.Printf("Have: %s\n", wrapped.Error())

	tests.AssertEqual(t, output.MatchString(wrapped.Error()), true)
	tests.AssertEqual(t, CombinedErrorType(wrapped), ETypeExec)
	tests.AssertEqual(t, errors.Unwrap(wrapped), err)
}

func TestWrapInType(t *testing.T) {
	err := New("internal error", ETypeExec)
	wrapped := WrapInType(err, ETypeFilesystem, "message1", "message2")

	output := regexp.MustCompile(`(?m)^message1 message2: internal error \[ (?:.+)srberrors/wrap_test.go:\d+ \(srberrors.TestWrapInType\) \]$`)

	log.Printf("Want: %s\n", output)
	log.Printf("Have: %s\n", wrapped.Error())

	tests.AssertEqual(t, output.MatchString(wrapped.Error()), true)
	tests.AssertEqual(t, CombinedErrorType(wrapped), ETypeExec|ETypeFilesystem)
	tests.AssertEqual(t, errors.Unwrap(wrapped), err)
	tests.AssertEqual(t, CombinedErrorType(err), ETypeExec)
}
