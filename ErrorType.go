package srberrors

import (
	"errors"
	"strings"
)

// ErrorType is a quick-hand to capture multiple Errors as one comparator for
// IsAnyType().
type ErrorType uint32

func (typ ErrorType) Matches(err error) bool {
	return IsAnyType(err, typ)
}

func (typ ErrorType) String() string {
	flagnames := []string{}

	if typ == ETypeNil {
		return "ETypeNil"
	}
	if typ&ETypeFileFormat == ETypeFileFormat {
		flagnames = append(flagnames, "ETypeFileFormat")
	}
	if typ&ETypeInternalStateMismatch == ETypeInternalStateMismatch {
		flagnames = append(flagnames, "ETypeInternalStateMismatch")
	}
	if typ&ETypeDeduplication == ETypeDeduplication {
		flagnames = append(flagnames, "ETypeDeduplication")
	}
	if typ&ETypeConfiguration == ETypeConfiguration {
		flagnames = append(flagnames, "ETypeConfiguration")
	}
	if typ&ETypeFFMpeg == ETypeFFMpeg {
		flagnames = append(flagnames, "ETypeFFMpeg")
	}
	if typ&ETypePlugin == ETypePlugin {
		flagnames = append(flagnames, "ETypePlugin")
	}
	if typ&ETypeExec == ETypeExec {
		flagnames = append(flagnames, "ETypeExec")
	}
	if typ&ETypeNetwork == ETypeNetwork {
		flagnames = append(flagnames, "ETypeNetwork")
	}
	if typ&ETypeListen == ETypeListen {
		flagnames = append(flagnames, "ETypeListen")
	}
	if typ&ETypeFilesystem == ETypeFilesystem {
		flagnames = append(flagnames, "ETypeFilesystem")
	}
	if typ&unusedEType10 == unusedEType10 {
		flagnames = append(flagnames, "unusedEType10")
	}
	if typ&unusedEType11 == unusedEType11 {
		flagnames = append(flagnames, "unusedEType11")
	}
	if typ&unusedEType12 == unusedEType12 {
		flagnames = append(flagnames, "unusedEType12")
	}
	if typ&unusedEType13 == unusedEType13 {
		flagnames = append(flagnames, "unusedEType13")
	}
	if typ&unusedEType14 == unusedEType14 {
		flagnames = append(flagnames, "unusedEType14")
	}
	if typ&unusedEType15 == unusedEType15 {
		flagnames = append(flagnames, "unusedEType15")
	}
	if typ&unusedEType16 == unusedEType16 {
		flagnames = append(flagnames, "unusedEType16")
	}
	if typ&unusedEType17 == unusedEType17 {
		flagnames = append(flagnames, "unusedEType17")
	}
	if typ&unusedEType18 == unusedEType18 {
		flagnames = append(flagnames, "unusedEType18")
	}
	if typ&unusedEType19 == unusedEType19 {
		flagnames = append(flagnames, "unusedEType19")
	}
	if typ&unusedEType20 == unusedEType20 {
		flagnames = append(flagnames, "unusedEType20")
	}
	if typ&unusedEType21 == unusedEType21 {
		flagnames = append(flagnames, "unusedEType21")
	}
	if typ&unusedEType22 == unusedEType22 {
		flagnames = append(flagnames, "unusedEType22")
	}
	if typ&unusedEType23 == unusedEType23 {
		flagnames = append(flagnames, "unusedEType23")
	}
	if typ&unusedEType24 == unusedEType24 {
		flagnames = append(flagnames, "unusedEType24")
	}
	if typ&unusedEType25 == unusedEType25 {
		flagnames = append(flagnames, "unusedEType25")
	}
	if typ&unusedEType26 == unusedEType26 {
		flagnames = append(flagnames, "unusedEType26")
	}
	if typ&unusedEType27 == unusedEType27 {
		flagnames = append(flagnames, "unusedEType27")
	}
	if typ&unusedEType28 == unusedEType28 {
		flagnames = append(flagnames, "unusedEType28")
	}
	if typ&unusedEType29 == unusedEType29 {
		flagnames = append(flagnames, "unusedEType29")
	}
	if typ&unusedEType30 == unusedEType30 {
		flagnames = append(flagnames, "unusedEType30")
	}
	if typ&ETypeUnflaggedExternal == ETypeUnflaggedExternal {
		flagnames = append(flagnames, "ETypeUnflaggedExternal")
	}

	return strings.Join(flagnames, "|")
}

func CombinedErrorType(err error) (typ ErrorType) {
	if err == nil {
		return ETypeNil
	}

	// We accumulate all ErrorType of nested errors.
	// That way two separate errors can be matched with a combined flag,
	// even if they on their own did not have both flags set, but their
	// combined flags match the requested type.
	for err != nil {
		if err2, ok := err.(Error); ok {
			typ |= err2.ErrorType
		} else if err2, ok := err.(*Error); ok {
			typ |= err2.ErrorType
		} else {
			typ |= ETypeUnflaggedExternal
		}
		err = errors.Unwrap(err)
	}

	return
}
