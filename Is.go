package srberrors

import (
	"errors"
)

// Is is a array compatible variant of errors.Is().
//
// You can pass in multiple errors to compare with. If any one matches
// this returns true, false otherwise.
func Is(err error, comparator ...error) bool {
	for _, comp := range comparator {
		if errors.Is(err, comp) {
			return true
		}
	}
	return false
}

func IsAnyType(err error, etype ...ErrorType) bool {
	errEtype := CombinedErrorType(err)

	for _, typ := range etype {
		if typ > ETypeNil && typ&errEtype == typ ||
			errEtype == ETypeNil && typ == ETypeNil {
			return true
		}
	}
	return false
}
