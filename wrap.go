package srberrors

import (
	"fmt"
	"path"
	"runtime"
	"strings"
)

// RecoverWithTrace is a helper function, which allows you to retrieve an error
// from a panic() wrapped in a trace where it was thrown.
//
// If there was no panic to recover from, the passed `in` error will be returned.
//
// It should be called like this in a deferred function:
//     if err = srberrors.RecoverWithTrace(recover(), err); err != nil {
//     	tx.Rollback()
//     }
//     return err
// If you do not need to handle the error, you can also just return it directly.
//     return srberrors.RecoverWithTrace(recover(), err)
func RecoverWithTrace(recovered interface{}, in error) (err error) {
	if recovered != nil {
		var ok bool

		// This captures the original error or the placeholder
		if err, ok = recovered.(error); ok {
			err = NestedWrap(err, 3, "panic")
		} else {
			err = NestedWrap(fmt.Errorf("%+v", recovered), 3, "panic")
		}

		// This leaves a trace to the call of this recover
		err = NestedWrap(err, 1)
	}
	if err == nil {
		err = in
	}
	return
}

// Wrap wraps an error with the context of the file and line this function
// is executed on.
//
// The wrapped error should be generated as close as possible. If possible
// the line above the call to this function, or preferably on the same line.
// This is helpful, to see exactly where an error originated without much
// effort.

// If you wrap an error from an external package etc., please use WrapInType()
// to also assign an ErrorType instead.
// This will help to classify errors throughout the application.
//
// The format of the error string is NOT stable and may be changed, but is
// guaranteed to include all `msg` values and the `Error()` output of the
// wrapped error.
//
// If the input is nil, nil will be returned.
func Wrap(in error, msg ...string) (out error) {
	return NestedWrap(in, 1, msg...)
}

// NestedWrap should not be used, unless you know what you are doing.
//
// It can adjust the level of traces to go back before wrapping.
// This can be useful for use in recover() situations, or when wrapping
// a panic() in another function.
//
// For the everyday wrap, please use Wrap() instead.
//
// The format of the error string is NOT stable and may be changed, but is
// guaranteed to include all `msg` values and the `Error()` output of the
// wrapped error.
//
// If the input is nil, nil will be returned.
func NestedWrap(in error, nestCount int, msg ...string) (out error) {
	if in == nil {
		return nil
	}
	file, line, function := GetCaller(1 + nestCount)

	if len(msg) > 0 {
		out = New(fmt.Sprintf("%s: %s [ %s:%d (%s) ]", strings.Join(msg, " "), in, file, line, function), CombinedErrorType(in))
	} else {
		out = New(fmt.Sprintf("%s [ %s:%d (%s) ]", in, file, line, function), CombinedErrorType(in))
	}
	out.(*Error).wrapped = in

	return
}

// WrapInType is similar to Wrap, and all of its notes apply.
//
// In addition, this function accepts an ErrorType for checking with IsAnyType().
//
// If you wrap an error from an external package etc., please use this and
// assign an ErrorType, instead of calling Wrap().
// This will help to classify errors throughout the application.
//
// If the input is nil, nil will be returned.
func WrapInType(err error, errorType ErrorType, msg ...string) (out error) {
	if err == nil {
		return nil
	}
	out = NestedWrap(err, 1, msg...)
	out.(*Error).ErrorType |= errorType
	return
}

// GetCaller returns the file, line and function of the caller of it.
//
// Adjust nestCount to the desired number of callers to ignore.
// A nestCount of 0 returns the very line this function is called in, whereas
// 1 would return the line of the caller of the function this function is
// called in.
func GetCaller(nestCount int) (file string, line int, function string) {
	var pc uintptr
	var ok bool
	pc, file, line, ok = runtime.Caller(1 + nestCount)
	if !ok {
		file = "<???>"
		line = 1
	}
	details := runtime.FuncForPC(pc)
	if details != nil {
		function = path.Base(details.Name())
	} else {
		function = "<???>"
	}
	return
}
