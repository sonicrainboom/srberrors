package srberrors

import (
	"errors"
)

// Error extends the regular Golang error by an ErrorType which can be used to
// classify shared of errors throughout the application.
type Error struct {
	error
	ErrorType
	wrapped error
}

// New creates a new error, with an associated ErrorType.
func New(text string, errorType ErrorType) *Error {
	return &Error{error: errors.New(text), ErrorType: errorType}
}

func (e *Error) Unwrap() error {
	return e.wrapped
}
