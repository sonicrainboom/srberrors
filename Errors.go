package srberrors

var ErrDedupeUnsupportedType = New("deduplication unsupported for supplied file type", ETypeDeduplication|ETypeFileFormat)
var ErrDedupeUnsupportedDepth = New("deduplication unsupported for supplied bit-depth", ETypeDeduplication|ETypeFileFormat)
var ErrDedupeUnsupportedFormat = New("deduplication unsupported for supplied sample format", ETypeDeduplication|ETypeFileFormat)
var ErrDedupeCorruptContext = New("deduplication context corrupted", ETypeDeduplication|ETypeInternalStateMismatch)
var ErrDedupeResultMismatch = New("deduplication did not create a deterministic result", ETypeDeduplication|ETypeInternalStateMismatch)
var ErrDedupeNoAudioStream = New("deduplication could not find an audio stream", ETypeDeduplication|ETypeFileFormat)
var ErrBlobCompressionInvalid = New("supplied compression method is unknown", ETypeConfiguration|ETypeInternalStateMismatch)
var ErrPluginPlatformMismatch = New("the supplied plugin can not be loaded on the current platform", ETypePlugin|ETypeExec|ETypeFileFormat)
var ErrPluginNoPlugin = New("the supplied binary is not a plugin", ETypePlugin|ETypeExec|ETypeFileFormat)

const (
	// ErrorType declarations (32 bit uint bitfield)
	ETypeNil                   ErrorType = 0       // Nil
	ETypeFileFormat            ErrorType = 1 << 0  // Errors that relate to a file being corrupted or not matching the expected format.
	ETypeInternalStateMismatch ErrorType = 1 << 1  // Errors that relate to possible bugs. This includes self-test errors.
	ETypeDeduplication         ErrorType = 1 << 2  // Errors that reflect issues deduplicating a given file
	ETypeConfiguration         ErrorType = 1 << 3  // Errors that reflect configuration issues
	ETypeFFMpeg                ErrorType = 1 << 4  // Errors executing FFMpeg related actions
	ETypePlugin                ErrorType = 1 << 5  // Unused field
	ETypeExec                  ErrorType = 1 << 6  // Unused field
	ETypeNetwork               ErrorType = 1 << 7  // Unused field
	ETypeListen                ErrorType = 1 << 8  // Unused field
	ETypeFilesystem            ErrorType = 1 << 9  // Unused field
	unusedEType10              ErrorType = 1 << 10 // Unused field
	unusedEType11              ErrorType = 1 << 11 // Unused field
	unusedEType12              ErrorType = 1 << 12 // Unused field
	unusedEType13              ErrorType = 1 << 13 // Unused field
	unusedEType14              ErrorType = 1 << 14 // Unused field
	unusedEType15              ErrorType = 1 << 15 // Unused field
	unusedEType16              ErrorType = 1 << 16 // Unused field
	unusedEType17              ErrorType = 1 << 17 // Unused field
	unusedEType18              ErrorType = 1 << 18 // Unused field
	unusedEType19              ErrorType = 1 << 19 // Unused field
	unusedEType20              ErrorType = 1 << 20 // Unused field
	unusedEType21              ErrorType = 1 << 21 // Unused field
	unusedEType22              ErrorType = 1 << 22 // Unused field
	unusedEType23              ErrorType = 1 << 23 // Unused field
	unusedEType24              ErrorType = 1 << 24 // Unused field
	unusedEType25              ErrorType = 1 << 25 // Unused field
	unusedEType26              ErrorType = 1 << 26 // Unused field
	unusedEType27              ErrorType = 1 << 27 // Unused field
	unusedEType28              ErrorType = 1 << 28 // Unused field
	unusedEType29              ErrorType = 1 << 29 // Unused field
	unusedEType30              ErrorType = 1 << 30 // Unused field
	ETypeUnflaggedExternal     ErrorType = 1 << 31 // Unknown Errors that have not been assigned a type. Ideally you should never see these.
)
